function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    if(typeof obj !== "object" || obj === null){
        return []
    }
    let pair = [];
    for(let key in obj){
        let val = [];
        val.push(key);
        val.push(obj[key])
        pair.push(val)
    }
    return pair
}
module.exports = pairs;


