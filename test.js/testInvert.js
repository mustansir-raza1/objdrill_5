//create a variable to imports the function from other directory
let fnInvert = require("../invert.js");

try{
    //create a variable to execute the function which is imported.
    let invertObject = fnInvert({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
    //it prints the output
    console.log(invertObject);
}
catch(error){
    console.log("error")
}


