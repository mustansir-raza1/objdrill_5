// create a variable to import the function from other directory
let fnDefault = require("../default.js");

try{
    //define a variable object with undefined values
    let objectValue = { a: undefined, b: undefined };
    
    //Define the object this object used as the defaults value
    let defaultValue = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
    
    //create the variable to store the data which is executed by function
    let updatedObject = fnDefault(objectValue, defaultValue);
    // printthe output
    console.log(updatedObject);
}
catch(error){
    console.log("error")
}

