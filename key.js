// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
function keyFunc(obj){
    if (typeof obj !== "object" || obj == null){
        return [];
    }
    let newKey = [];
    for(let key in obj){
        newKey.push(key)
    }
    return newKey
}
module.exports = keyFunc
