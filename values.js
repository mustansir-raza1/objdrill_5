// Return all of the values of the object's own properties.
// Ignore functions

function values(obj) {
    if (typeof obj == 'object') {
        let values = [];
        for( key in obj){
            values.push(obj[key]);
        }
        return values;
    }
    else{
        return []
    }
    
}
module.exports = values;

