// mapObject.js
function mapObject(obj, cb) {
    if(typeof obj !== "object" || obj == null){
        return [];
    }
    // Initialize an empty object to store the result
    const result = {};
    // Iterate over each property of the input object
    for (const key in obj) {
        // Apply the callback function to the property value and store the result
        result[key] = cb(obj[key]);
    }
    // Return the transformed object
    return result;
}

module.exports = mapObject;
